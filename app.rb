module App
  class API < Sinatra::Base

    register Sinatra::CrossOrigin

    configure :development, :production do
      set :server, %w[thin mongrel webrick]
      set :bind, '0.0.0.0'
      set :port, 5000

      enable :logging
    end

    configure :development do
      enable :cross_origin
      set :allow_origin, :any
      set :allow_methods, %i[get post options]
      set :allow_credentials, true
      set :max_age, '1728000'
      set :expose_headers, ['Content-Type']

      set :show_exceptions, :after_handler
    end

    not_found do
      'resource not found'
    end

    error do
      'error'
    end

    options '*' do
      response.headers["Allow"] = "HEAD,GET,PUT,POST,DELETE,OPTIONS"

      response.headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept"
      200
    end

    get '/graphql/?' do
      content_type 'application/json'

      query = GraphQL::Introspection::INTROSPECTION_QUERY

      results = GraphQL::Types::Coursera::Schema.execute(query)

      status 200
      body JSON.pretty_generate(results)
    end

    post '/graphql/?' do
      query = ''

      case request.content_type
      when 'application/graphql'
        query = request.body.read
      when 'application/json'
        body = request.body.read
        json = JSON.parse(body)
        query = json['query']
      else
        raise Sinatra::NotFound
      end

      results = GraphQL::Types::Coursera::Schema.execute(query)

      content_type 'application/json'
      status 200
      body JSON.pretty_generate(results)
    end

    run! if app_file == $PROGRAM_NAME
  end
end
