namespace :docker do
  desc 'build docker image'
  task :build do
    tag = 'develop'

    case SERVER_ENV
    when 'production'
      tag = VERSION
    end

    system("docker build -t #{DOCKERREPO}/#{NAME}:#{tag} .")
  end

  desc 'clean docker image'
  task :clean do
    tag = 'develop'

    case SERVER_ENV
    when 'production'
      tag = VERSION
    end

    system("docker rmi #{DOCKERREPO}/#{NAME}:#{tag} .")
  end
end
