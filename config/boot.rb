require 'bundler/setup'
require 'sinatra/cross_origin'
require 'graphql/schema/coursera'
require 'json'

require './app'
