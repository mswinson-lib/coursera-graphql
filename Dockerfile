FROM ruby:latest

ADD . /var/services/coursera-graphql
WORKDIR /var/services/coursera-graphql

RUN ./scripts/setup
CMD ./scripts/init
